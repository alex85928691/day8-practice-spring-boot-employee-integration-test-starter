package com.afs.restapi;

import com.afs.restapi.Entity.Employee;
import com.afs.restapi.Repository.EmployeeRepository;
import com.afs.restapi.Service.EmployeeService;
import com.afs.restapi.exception.EmployeeAgeErrorException;
import com.afs.restapi.exception.EmployeeIsInactiveException;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
public class EmployeeServiceTest {

    private final EmployeeRepository employeeRepository = mock(EmployeeRepository.class);

    private final EmployeeService employeeService = new EmployeeService(employeeRepository);
    @Test
    void should_throw_invalid_when_create_given_employee_age_less_than_18_and_age66() {
        //given
        Employee newEmployee1 = new Employee("Tom",16,"Male",1000);
        Employee newEmployee2 = new Employee("Susan",66,"Female",5000);
        //when

        //then
        assertThrows(EmployeeAgeErrorException.class, () -> employeeService.create(newEmployee1));
        assertThrows(EmployeeAgeErrorException.class, () -> employeeService.create(newEmployee2));
    }

    @Test
    void should_add_new_employee_when_create_given_employee_age_in_range() {
        Employee employee = new Employee( "Tom", 35, "Male", 1000);

        employeeService.create(employee);

        verify(employeeRepository, times(1)).insert(employee);
    }

    @Test
    void should_return_employee_with_status_true_when_create_given_employee() {
        //given
        Employee employee = new Employee( 1L,"Tom", 37, "Male", 1000);
        Employee employeeReturn = new Employee( 1L,"Tom", 37, "Male", 1000);
        when(employeeRepository.insert(any())).thenReturn(employeeReturn);
        employeeReturn.setStatus(true);
        //when
        Employee employeeSaved = employeeService.create(employee);
        //then
        assertTrue(employeeSaved.isStatus());
        verify(employeeRepository).insert(argThat(employeeToSave -> {
            assertTrue(employeeToSave.isStatus());
            return true;
        }));
    }

    @Test
    void should_set_status_false_when_delete_given_employee_id() {
        //given
        Employee employee = new Employee( 1L,"Tom", 37, "Male", 1000,true);
        when(employeeRepository.findById(anyLong())).thenReturn(employee);
        //when
        employeeService.delete(employee.getId());
        //then
        assertFalse(employee.isStatus());
    }

    @Test
    void should_update_when_update_given_the_employee() {
        //given
        Employee employee = new Employee(1L,"Tom", 37, "Male", 2000,true);
        Employee employeeSaved = new Employee( 1L,"Tom", 37, "Male", 1000);
        //when
        employeeService.create(employeeSaved);
        when(employeeRepository.update(anyLong(),any())).thenReturn(employee);

        //then
        employeeService.update(1L,employee);

        verify(employeeRepository, times(1)).update(employeeSaved.getId(),employee);
    }

   @Test
   void should_not_update_when_update_given_the_left_employee() {
       //given
       Employee employee = new Employee(1L,"Tom", 37, "Male", 2000,false);
       //when
       when(employeeRepository.update(anyLong(),any())).thenReturn(employee);

       //then
       assertThrows(EmployeeIsInactiveException.class,()->{
           employeeService.update(1L,employee);
       });
   }

   @Test
   void should_find_by_page_when_findbypage_given_pagenumber_and_pagesize() {
       //given
       employeeService.findByPage(1,2);
       //when
       verify(employeeRepository, times(1)).findByPage(1,2);
       //then
   }

   @Test
   void should_find_by_gender_when_findbygender_given_male() {
       employeeService.findByGender("Female");
       // then
       verify(employeeRepository, times(1)).findByGender("Female");
   }

   @Test
   void should_run_1_time_when_findbyid_given_id() {
       //given
       employeeService.findById(1L);

       // then
       verify(employeeRepository, times(1)).findById(1L);
       //when
       //then
   }

   @Test
   void should_run_1_time_when_getEmployeeList_given_getEmployeeList() {
       employeeService.getEmployeeList();

       // then
       verify(employeeRepository, times(1)).findAll();
       //when
   }


}
