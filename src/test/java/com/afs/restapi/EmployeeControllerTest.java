package com.afs.restapi;

import com.afs.restapi.Entity.Employee;
import com.afs.restapi.Repository.EmployeeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class EmployeeControllerTest {
	@Autowired
	MockMvc postmanMock;
	@Autowired
	EmployeeRepository employeeRepository;

	@BeforeEach
	void setup(){
		employeeRepository.reset();
	}
	@Test
	void should_return_all_employees_when_perform_get_given_employees() throws Exception {
	    //given

	    //when
		postmanMock.perform(MockMvcRequestBuilders.get("/employees"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$",hasSize(5)));

	    //then
	}

	@Test
	void should_return_employee_id_1_when_perform_get_by_id_given_id() throws Exception {
	    //given
	    
	    //when
		postmanMock.perform(MockMvcRequestBuilders.get("/employees/1"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1L))
				.andExpect(MockMvcResultMatchers.jsonPath("$.name").value("John Smith"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.age").value(32))
				.andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("Male"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(5000));
	}
	
	@Test
	void should_delete_employee_id_1_when_perform_delete_with_id_given_id() throws Exception {
		postmanMock.perform(MockMvcRequestBuilders.delete("/employees/1"))
				.andExpect(MockMvcResultMatchers.status().isNoContent());

		assertEquals(false, employeeRepository.findById(1L).isStatus());

	}

	@Test
	void should_return_all_male_when_perform_get_by_gender_given_gender() throws Exception {
	    //given
	    postmanMock.perform(MockMvcRequestBuilders.get("/employees").param("gender","Male"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$",hasSize(3)))
				.andExpect(MockMvcResultMatchers.jsonPath("$..gender").value(everyItem(is("Male"))));

	}

	@Test
	void should_return_correct_size_when_perform_get_with_page_and_size_given_pagenumber_and_pagesize() throws Exception {
	    //given
		postmanMock.perform(MockMvcRequestBuilders.get("/employees").param("pageNumber","1")
				.param("pageSize","2"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$",hasSize(2)))
				.andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(1L))
				.andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("John Smith"))
				.andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(32))
				.andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value("Male"))
				.andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").value(5000))
				.andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(2L))
				.andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value("Jane Johnson"))
				.andExpect(MockMvcResultMatchers.jsonPath("$[1].age").value(28))
				.andExpect(MockMvcResultMatchers.jsonPath("$[1].gender").value("Female"))
				.andExpect(MockMvcResultMatchers.jsonPath("$[1].salary").value(6000));
	}

	
	@Test
	void should_return_new_employee_when_perform_post_given_new_employee() throws Exception {
	    //given
		String newEmployee = "{\n" +
				"\"name\": \"Jim\",\n" +
				"\"age\": 20,\n" +
				"\"gender\": \"Male\",\n" +
				"\"salary\": 55000\n" +
				"}";
	    //when
		postmanMock.perform(MockMvcRequestBuilders.post("/employees")
				.contentType(MediaType.APPLICATION_JSON).content(newEmployee))
				.andExpect(MockMvcResultMatchers.status().isCreated())
				.andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Jim"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.age").value(20))
				.andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("Male"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(55000))
				.andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty());

		List<Employee> allList = employeeRepository.findAll();
		assertEquals(6,allList.size());
		assertEquals("Jim",allList.get(allList.size()-1).getName());
	    //then
	}

	@Test
	void should_return_updated_employee_when_perform_updated_given_new_information() throws Exception {
		//given
		employeeRepository.clearAll();
		ObjectMapper mapper = new ObjectMapper();
		Employee employeeSusan = new Employee(1L, "Susan", 22, "Female", 10000, true);
		employeeRepository.insert(employeeSusan);
		Employee toBeUpdateSusan = new Employee(1L, "Susan", 22, "Female", 10000, true);
		toBeUpdateSusan.setSalary(20000);
		String susanJson = mapper.writeValueAsString(toBeUpdateSusan);

		//when
		postmanMock.perform(MockMvcRequestBuilders.put("/employees/{id}", 1)
						.contentType(MediaType.APPLICATION_JSON)
						.content(susanJson))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(1))
				.andExpect(jsonPath("$.name").value("Susan"))
				.andExpect(jsonPath("$.age").value(22))
				.andExpect(jsonPath("$.gender").value("Female"))
				.andExpect(jsonPath("$.salary").value(20000));

		Employee suSanInRepo = employeeRepository.findById(1L);
		assertEquals(toBeUpdateSusan.getSalary(), suSanInRepo.getSalary());
		//then
	}

	@Test
	void should_return_404_when_perform_get_by_id_given_id() throws Exception {
		postmanMock.perform(MockMvcRequestBuilders.get("/employees/99"))
				.andExpect(MockMvcResultMatchers.status().isNotFound());

	}

	@Test
	void should_200_when_perform_delete_with_id_given_id() throws Exception {
		postmanMock.perform(MockMvcRequestBuilders.delete("/employees/1"))
				.andExpect(MockMvcResultMatchers.status().isNoContent());

		postmanMock.perform(MockMvcRequestBuilders.get("/employees/1"))
				.andExpect(MockMvcResultMatchers.status().isOk());

	}

	@Test
	void should_404_when_perform_insert_with_id_given_id() throws Exception {
				String newEmployee = "{\n" +
				"\"name\": \"Jim\",\n" +
				"\"age\": 20,\n" +
				"\"gender\": \"Male\",\n" +
				"\"salary\": 55000\n" +
				"}";

		postmanMock.perform(MockMvcRequestBuilders.post("/employee/1")
				.contentType(MediaType.APPLICATION_JSON).content(newEmployee))
				.andExpect(MockMvcResultMatchers.status().isNotFound());
	}

	@Test
	void should_throw_404_when_perform_update_with_id_given_id() throws Exception {
		String newEmployee = "{\n" +
				"\"name\": \"Jim\",\n" +
				"\"age\": 20,\n" +
				"\"gender\": \"Male\",\n" +
				"\"salary\": 55000\n" +
				"}";

		postmanMock.perform(MockMvcRequestBuilders.put("/employee/99")
						.contentType(MediaType.APPLICATION_JSON).content(newEmployee))
				.andExpect(MockMvcResultMatchers.status().isNotFound());
	}



}
