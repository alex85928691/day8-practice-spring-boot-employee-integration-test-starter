package com.afs.restapi.Service;

import com.afs.restapi.Entity.Employee;
import com.afs.restapi.Repository.EmployeeRepository;
import com.afs.restapi.exception.EmployeeAgeErrorException;
import com.afs.restapi.exception.EmployeeIsInactiveException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }


    public Employee create(Employee employee) {
        if(employee.getAge() <18 || employee.getAge() >65) throw new EmployeeAgeErrorException();
        employee.setStatus(true);
        return employeeRepository.insert(employee);
    }

    public void delete(Long id) {
        Employee employee = employeeRepository.findById(id);
        if(employee !=null){
            employee.setStatus(false);
            employeeRepository.update(id,employee);
        }

    }

    public Employee update(Long id, Employee employee) {

        if (!employee.isStatus()) {
            throw new EmployeeIsInactiveException();
        }
        return employeeRepository.update(id, employee);
    }

    public List<Employee> getEmployeeList() {
        return employeeRepository.findAll();
    }

    public Employee findById(Long id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> findByGender(String gender) {
        return  employeeRepository.findByGender(gender);
    }

    public List<Employee> findByPage(Integer pageNumber, Integer pageSize) {
        return employeeRepository.findByPage(pageNumber,pageSize);
    }
}
